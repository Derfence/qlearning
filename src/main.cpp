#include <iostream>
#include <time.h>
#include <omp.h>
#include "QL.hpp"

using namespace std;

int main(int argc, char **argv)
{
    if (argc <= 2)
        return 1;

    srand(time(NULL));
    //Creation of an instance
    unsigned gridSize = 5 + rand() % 10;
    Agent agent = Agent(gridSize);
    //argmax function
    auto argmax = [](Row row) -> unsigned { return distance(row.begin(), max_element(row.begin(), row.end())); };
    //eps-greedy
    auto epsGreedy = [&agent, &argmax](unsigned stateId, double eps) -> unsigned {
        if (rand() / RAND_MAX < eps)
            return rand() % 4;
        return argmax(agent.getRow(stateId));
    };

    //QL
    Solution sol = QL::Resolve(agent, atoi(argv[1]), atof(argv[2]));

    //Print
    string c, inSol;
    for (int y = gridSize - 1; y >= 0; y--)
    {
        for (unsigned x = 0; x < gridSize; x++)
        {
            if (find(sol.begin(), sol.end(), y * gridSize + x) != sol.end())
                inSol = "\033[34m";
            else
                inSol = "";
            unsigned action = epsGreedy(y * gridSize + x, .0);
            switch (action)
            {
            case 0:
                c = "\033[33m" + inSol + "l\033[0m";
                break;
            case 1:
                c = "\033[32m" + inSol + "u\033[0m";
                break;
            case 2:
                c = "\033[32m" + inSol + "d\033[0m";
                break;
            case 3:
                c = "\033[33m" + inSol + "r\033[0m";
                break;

            default:
                break;
            }
            if (agent.reward(x, y) == 1)
                c = "\033[34m" + inSol + "O\033[0m";
            if (agent.reward(x, y) == -1)
                c = "\033[31m" + inSol + "X\033[0m";
            cout << c << " ";
        }
        cout << endl;
    }

    return 0;
}