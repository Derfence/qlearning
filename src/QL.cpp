#include "QL.hpp"

vector<vector<int>> QL::actions = {
    {-1, 0} /*Left*/,
    {0, 1} /*Up*/,
    {0, -1} /*Down*/,
    {1, 0} /*Right*/
};