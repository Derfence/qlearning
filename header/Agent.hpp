#ifndef AGENT_HPP
#define AGENT_HPP

#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <cstdlib>
#include <vector>

using namespace std;
typedef boost::numeric::ublas::mapped_matrix<int> SparseMatrix;
typedef boost::numeric::ublas::matrix<double> Matrix;
typedef boost::numeric::ublas::matrix_row<Matrix> Row;

class Agent
{
public:
    SparseMatrix reward;
    Matrix Q;
    vector<unsigned> pos;
    unsigned gridSize;

    Row getRow(unsigned index) { return Row(Q, index); }

    Agent(unsigned gridSize) : reward(SparseMatrix(gridSize, gridSize)), Q(Matrix(gridSize * gridSize, 4 /*nb d'actions*/)), pos({rand() % gridSize, rand() % gridSize}), gridSize(gridSize)
    {
        //Final State
        reward(rand() % gridSize, rand() % gridSize) = 1;
        //Random malus
        unsigned nbMalus = rand() % (gridSize * 2);
        for (unsigned i = 0; i < nbMalus; i++)
        {
            unsigned x = rand() % gridSize, y = rand() % gridSize;
            reward(x, y) = reward(x, y) == 1 ? 1 : -1; // = 1 if final state, = -1 otherwise
        }
    }

    ~Agent() {}
};

#endif