#ifndef SA_HPP
#define SA_HPP

#include <numeric>
#include <cmath>
#include "Agent.hpp"

using namespace std;

//Each cell id is y * gridSize + x
typedef vector<unsigned> Solution;

class QL
{
public:
    //Actions
    static vector<vector<int>> actions;
    static Solution Resolve(Agent &agent, unsigned iterMax, double eps)
    {
        Solution sol = Solution();
        sol.push_back(agent.pos[1] * agent.gridSize + agent.pos[0]);

        //Init
        double alpha = 0.1, gamma = 0.75;

        //argmax function
        auto argmax = [](Row row) -> unsigned { return distance(row.begin(), max_element(row.begin(), row.end())); };
        //eps-greedy
        auto epsGreedy = [&agent, &argmax](unsigned stateId, double eps) -> unsigned {
            if (rand() / RAND_MAX < eps)
                return rand() % 4;
            return argmax(agent.getRow(stateId));
        };

        //Qlearning
        unsigned iter = 0;
        unsigned stateId = agent.pos[1] * agent.gridSize + agent.pos[0];
        unsigned action, nextStateId, nextBestAction;
        vector<unsigned> nextState;
        int nextReward;
        while (iter < iterMax)
        {
            iter++;
            action = epsGreedy(stateId, eps);

            nextState = agent.pos;
            nextState[0] = max(0, min<int>(agent.gridSize - 1, nextState[0] + QL::actions[action][0]));
            nextState[1] = max(0, min<int>(agent.gridSize - 1, nextState[1] + QL::actions[action][1]));
            nextReward = agent.reward(nextState[0], nextState[1]);
            nextStateId = nextState[1] * agent.gridSize + nextState[0];
            nextBestAction = epsGreedy(nextStateId, .0);

            agent.Q(stateId, action) += alpha * (nextReward + gamma * agent.Q(nextStateId, nextBestAction) - agent.Q(stateId, action));
            agent.pos = nextState;
            stateId = nextStateId;
        }

        //Compute solution
        unsigned x = sol[0] % agent.gridSize, y = (sol[0] - x) / agent.gridSize;
        unsigned i = 0;
        while (agent.reward(x, y) != 1)
        {
            unsigned action = epsGreedy(sol[i++], .0);
            x = max<unsigned>(0, min(agent.gridSize - 1, x + QL::actions[action][0]));
            y = max<unsigned>(0, min(agent.gridSize - 1, y + QL::actions[action][1]));
            sol.push_back(y * agent.gridSize + x);
        }

        return sol;
    }
};

#endif